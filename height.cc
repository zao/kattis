#include <algorithm>
#include <iostream>
#include <vector>

int
main()
{
  int n;
  std::cin >> n;
  for (int cse = 0; cse < n; ++cse) {
    int cseId;
    std::cin >> cseId;
    std::vector<int> heights;
    heights.reserve(20);
    int steps = 0;
    for (int i = 0; i < 20; ++i) {
      int height;
      std::cin >> height;
      auto I = std::lower_bound(heights.begin(), heights.end(), height);
      steps += std::distance(I, heights.end());
      heights.insert(I, height);
    }
    std::cout << cseId << " " << steps << std::endl;
  }
}
