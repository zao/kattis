#include <algorithm>
#include <cmath>
#include <iostream>
#include <iterator>
#include <string>

enum
{
  N_ = 0,
  N0 = 1,
  N1 = 2,
  N2 = 4,
  N3 = 8,
  N4 = 16,
  N5 = 32,
  N6 = 64,
  N7 = 128,
  N8 = 256,
  N9 = 512,
};

// ### ### ### ### ### ### ### ### ### ###
// ***   * *** *** * * *** *** *** *** *** __*: 1  *_*: 4  ***: 0,2,3,5,6,7,8,9
// * *   *   *   * * * *   *     * * * * * __*: 1,2,3,7  *_*: 0,4,8,9  *__: 5,6
// * *   * *** *** *** *** ***   * *** *** __*: 1,7  *_*: 0  ***: 2,3,4,5,6,8,9
// * *   * *     *   *   * * *   * * *   * __*: 1,3,4,5,7,9  *_*: 0,6,8  *__: 2
// ***   * *** ***   * *** ***   * *** *** __*: 1,4,7  ***: 0,2,3,5,6,8,9

// 0    1    2    3    4    5    6    7
// ___, __*, _*_, _**, *__, *_*, **_, ***

int
main()
{
  int lookup[5][8] = {};
  lookup[0][0b001] = N1;
  lookup[0][0b101] = N4;
  lookup[0][0b111] = N0 | N2 | N3 | N5 | N6 | N7 | N8 | N9;
  lookup[1][0b001] = N1 | N2 | N3 | N7;
  lookup[1][0b101] = N0 | N4 | N8 | N9;
  lookup[1][0b100] = N5 | N6;
  lookup[2][0b001] = N1 | N7;
  lookup[2][0b101] = N0;
  lookup[2][0b111] = N2 | N3 | N4 | N5 | N6 | N8 | N9;
  lookup[3][0b001] = N1 | N3 | N4 | N5 | N7 | N9;
  lookup[3][0b101] = N0 | N6 | N8;
  lookup[3][0b100] = N2;
  lookup[4][0b001] = N1 | N4 | N7;
  lookup[4][0b111] = N0 | N2 | N3 | N5 | N6 | N8 | N9;

  std::vector<int> masks[5];
  std::string line;
  for (int i = 0; i < 5; ++i) {
    std::getline(std::cin, line);
    for (int j = 0; j < line.size(); j += 4) {
      int code = 4 * (line[j] != ' ') + 2 * (line[j + 1] != ' ') +
                 1 * (line[j + 2] != ' ');
      int mask = lookup[i][code];
      masks[i].push_back(mask);
    }
  }
  int nSymbols = masks[0].size();
  int number = 0;
  for (int j = 0; j < nSymbols; ++j) {
    int mask = ~0;
    for (int i = 0; i < 5; ++i) {
      mask &= masks[i][j];
    }
    if (mask == 0 || (mask & (mask - 1))) {
      std::cout << "BOOM!!\n";
      return 0;
    }
    number *= 10;
    number += std::log2(mask);
  }
  if (number % 6) {
    std::cout << "BOOM!!\n";
  } else {
    std::cout << "BEER!!\n";
  }
}
