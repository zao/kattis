#include <algorithm>
#include <iostream>
#include <string>

char
Adrian(int idx)
{
  static char arr[] = "ABC";
  return arr[idx % 3];
}

char
Bruno(int idx)
{
  static char arr[] = "BABC";
  return arr[idx % 4];
}

char
Goran(int idx)
{
  static char arr[] = "CCAABB";
  return arr[idx % 6];
}

int
main()
{
  int n;
  std::cin >> n;
  std::string answers;
  std::cin >> answers;
  int rights[3] = {};
  for (int i = 0; i < n; ++i) {
    if (Adrian(i) == answers[i])
      ++rights[0];
    if (Bruno(i) == answers[i])
      ++rights[1];
    if (Goran(i) == answers[i])
      ++rights[2];
  }

  int bestScore = (std::max)(rights[0], (std::max)(rights[1], rights[2]));
  std::cout << bestScore << std::endl;
  for (int i = 0; i < 3; ++i) {
    char const* names[] = { "Adrian", "Bruno", "Goran" };
    if (rights[i] == bestScore) {
      std::cout << names[i] << std::endl;
    }
  }
}
