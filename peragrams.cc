#include <iostream>
#include <map>

int
main()
{
  std::map<char, int> freq;
  std::string s;
  std::cin >> s;
  for (auto ch : s) {
    freq[ch]++;
  }
  int odds = 0;
  for (auto& p : freq) {
    if (p.second % 2)
      ++odds;
  }
  int n = 0;
  if (odds > 1) {
    n = odds - 1;
  }
  std::cout << n << std::endl;
}
