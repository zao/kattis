#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

int
main()
{
  int t;
  std::cin >> t;
  for (int cse = 1; cse <= t; ++cse) {
    std::string srcNum, src, dst;
    std::cin >> srcNum >> src >> dst;
    int srcBase = src.size();
    int dstBase = dst.size();
    int dec = 0;
    for (auto ch : srcNum) {
      dec *= srcBase;
      dec += src.find_first_of(ch);
    }
    std::vector<char> dstNum;
    while (dec) {
      int lsd = dec % dstBase;
      dec /= dstBase;
      dstNum.push_back(dst[lsd]);
    }
    printf("Case #%d: ", cse);
    std::copy(dstNum.rbegin(), dstNum.rend(),
              std::ostream_iterator<char>(std::cout));
    printf("\n");
  }
}
