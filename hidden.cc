#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <vector>

int
main()
{
  std::string p, s;
  std::cin >> p >> s;
  std::queue<char> pQueue;
  for (char ch : p)
    pQueue.push(ch);
  std::multiset<char> pSet(p.begin(), p.end());
  for (char ch : s) {
    if (pQueue.empty())
      break;
    if (ch == pQueue.front()) {
      pQueue.pop();
      pSet.erase(pSet.find(ch));
    } else {
      if (pSet.find(ch) != pSet.end()) {
        break;
      }
    }
  }
  if (pQueue.empty()) {
    std::cout << "PASS\n";
  } else {
    std::cout << "FAIL\n";
  }
}
