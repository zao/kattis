#include <iostream>

char const* outputs[] = { "none", "one", "both" };

int
main()
{
  int a, b, c, d;
  std::cin >> a >> b >> c >> d;
  int men[3];
  std::cin >> men[0] >> men[1] >> men[2];
  int dogPeriod0 = a + b;
  int dogPeriod1 = c + d;
  for (int i = 0; i < 3; ++i) {
    int t0 = (men[i] - 1) % dogPeriod0;
    int t1 = (men[i] - 1) % dogPeriod1;
    int aggr0 = t0 < a;
    int aggr1 = t1 < c;
    std::cout << outputs[aggr0 + aggr1] << std::endl;
  }
}
