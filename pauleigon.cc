#include <iostream>

int
main()
{
  int n, p, q;
  std::cin >> n >> p >> q;
  int totalScore = p + q;
  int player = (totalScore / n) % 2;
  std::cout << (player ? "opponent" : "paul") << std::endl;
}
