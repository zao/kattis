#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <vector>

int
main()
{
  int n;
  std::cin >> n;
  std::cin.ignore(5, '\n');
  for (int cse = 0; cse < n; ++cse) {
    std::array<bool, 'z' - 'a' + 1> lut = {};
    std::string line;
    std::getline(std::cin, line);
    for (auto ch : line) {
      if (std::isupper(ch)) {
        lut[ch - 'A'] = true;
      } else if (std::islower(ch)) {
        lut[ch - 'a'] = true;
      }
    }
    if (std::all_of(begin(lut), end(lut), [](bool b) { return b; })) {
      std::cout << "pangram\n";
    } else {
      std::cout << "missing ";
      for (int i = 0; i < lut.size(); ++i) {
        if (!lut[i])
          std::cout << (char)(i + 'a');
      }
      std::cout << std::endl;
    }
  }
}
