#include <iostream>

int
main()
{
  int t;
  std::cin >> t;
  for (int cse = 0; cse < t; ++cse) {
    int d, m;
    std::cin >> d >> m;
    int weekday = 0;
    int fri13s = 0;
    for (int i = 0; i < m; ++i) {
      int days;
      std::cin >> days;
      if (days >= 13) {
        if ((weekday + 12) % 7 == 5) {
          ++fri13s;
        }
      }
      weekday += (days % 7);
    }
    std::cout << fri13s << std::endl;
  }
}
