#include <iostream>
#include <string>

int
main()
{
  char alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ_.";
  int alphabetSize = sizeof(alphabet) - 1;

  int n;
  std::string text;
  while (std::cin >> n && n) {
    std::cin >> text;
    for (auto I = text.rbegin(); I != text.rend(); ++I) {
      char ch = *I;
      int idx;
      if (ch >= 'A' && ch <= 'Z') {
        idx = ch - 'A';
      } else if (ch == '.') {
        idx = alphabetSize - 1;
      } else {
        idx = alphabetSize - 2;
      }
      std::cout << alphabet[(idx + n) % alphabetSize];
    }
    std::cout << std::endl;
  }
}
