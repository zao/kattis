#include <algorithm>
#include <iostream>
#include <string>

int
main()
{
  char const* ops['z' - 'a' + 1] = {
    "2",    "22", "222", "3",   "33", "333", "4",   "44",   "444",
    "5",    "55", "555", "6",   "66", "666", "7",   "77",   "777",
    "7777", "8",  "88",  "888", "9",  "99",  "999", "9999",
  };
  int n;
  std::cin >> n;
  std::cin.ignore(5, '\n');
  for (int cse = 1; cse <= n; ++cse) {
    std::string line;
    std::getline(std::cin, line);
    printf("Case #%d: ", cse);
    char lastKey = '\0';
    for (auto ch : line) {
      char const* str;
      if (ch == ' ') {
        str = "0";
      } else {
        str = ops[ch - 'a'];
      }
      int key = str[0];
      printf("%s%s", key == lastKey ? " " : "", str);
      lastKey = key;
    }
    printf("\n");
  }
}
