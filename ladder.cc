#include <cmath>
#include <iostream>

int
main()
{
  int h, v;
  std::cin >> h >> v;
  double hyp = h / sin(3.141593 * v / 180.0);
  std::cout << std::ceil(hyp) << std::endl;
}
