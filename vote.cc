#include <iostream>
#include <iterator>
#include <map>
#include <vector>

int
main()
{
  int t;
  std::cin >> t;
  for (int cse = 1; cse <= t; ++cse) {
    int n;
    std::cin >> n;
    std::multimap<int, int> votes;
    int sum = 0;
    for (int i = 0; i < n; ++i) {
      int count;
      std::cin >> count;
      votes.emplace(count, i);
      sum += count;
    }
    int topAmount = votes.rbegin()->first;
    if (votes.count(topAmount) > 1) {
      printf("no winner\n");
    } else {
      int winnerIdx = votes.rbegin()->second;
      fprintf(stderr, "%d votes, %d total\n", topAmount, sum);
      if (topAmount * 2 > sum) {
        printf("majority winner %d\n", winnerIdx + 1);
      } else {
        printf("minority winner %d\n", winnerIdx + 1);
      }
    }
  }
}
