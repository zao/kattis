#include <cmath>
#include <iostream>

double pi = 3.14159265359;

int
main()
{
  double r, x, y;
  while (std::cin >> r >> x >> y) {
    double q2 = x * x + y * y;
    double r2 = r * r;
    if (q2 >= r2) {
      printf("miss\n");
      continue;
    }
    double circleArea = pi * r2;
    double q = std::sqrt(q2);
    double theta = 2.0 * std::acos(q / r);
    double sectorArea = r2 / 2 * (theta - std::sin(theta));
    printf("%0.9f %0.9f\n", circleArea - sectorArea, sectorArea);
  }
}
