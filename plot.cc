#include <algorithm>
#include <cmath>
#include <iostream>
#include <iterator>
#include <vector>

int
Binomial(int n, int r)
{
  int nf = 1;
  for (int i = 2; i <= n; ++i)
    nf *= i;
  int rf = 1;
  for (int i = 2; i <= r; ++i)
    rf *= i;
  int nrf = 1;
  for (int i = 2; i <= n - r; ++i)
    nrf *= i;
  int num = nf;
  int den = rf * nrf;
  return num / den;
}

int
EvaluatePolynomial(std::vector<int> const& factors, int x)
{
  int sum = 0;
  int i = 0;
  for (auto a : factors) {
    sum += a * std::pow(x, i);
    ++i;
  }
  return sum;
}

// t_0:
// 1*C0 0*C1 0*C2 0*C3 0*C4 0*C5 0*C6 = a0 + a1*x^0 + a2*x^0 + a3*x^0 + a4*x^0 +
// a5*x^0 + a6*x^0
// 1*C0 1*C1 0*C2 0*C3 0*C4 0*C5 0*C6 = a0 + a1*x^1 + a2*x^1 + a3*x^1 + a4*x^1 +
// a5*x^1 + a6*x^1
// 1*C0 2*C1 1*C2 0*C3 0*C4 0*C5 0*C6 = a0 + a1*x^2 + a2*x^2 + a3*x^2 + a4*x^2 +
// a5*x^2 + a6*x^2
// 1*C0 3*C1 3*C2 1*C3 0*C4 0*C5 0*C6 = a0 + a1*x^3 + a2*x^3 + a3*x^3 + a4*x^3 +
// a5*x^3 + a6*x^3
// 1*C0 4*C1 6*C2 4*C3 1*C4 0*C5 0*C6 = a0 + a1*x^4 + a2*x^4 + a3*x^4 + a4*x^4 +
// a5*x^4 + a6*x^4
// 1*C0 5*C1 10*C2 10*C3 5*C4 1*C5 0*C6 = a0 + a1*x^5 + a2*x^5 + a3*x^5 + a4*x^5
// + a5*x^5 + a6*x^5
// 1*C0 6*C1 15*C2 20*C3 15*C4 6*C5 1*C6 = a0 + a1*x^6 + a2*x^6 + a3*x^6 +
// a4*x^6 + a5*x^6 + a6*x^6

int
main()
{
  int n;
  std::cin >> n;
  n += 1;
  std::vector<int> as(n);
  std::copy_n(std::istream_iterator<int>(std::cin), n, as.rbegin());

  std::vector<int> cs(n);
  cs[0] = as[0];

  std::vector<int> ps(n);
  ps[0] = as[0];

  for (int i = 1; i < n; ++i) {
    int pI = EvaluatePolynomial(as, i);
    ps[i] = pI;
    int cI = ps[i];
    for (int j = 0; j < n; ++j) {
      int f = Binomial(i, j);
      cI -= f * cs[j];
    }
    cs[i] = cI;
  }
  std::copy(cs.begin(), cs.end(), std::ostream_iterator<int>(std::cout, " "));
}
