#include <algorithm>
#include <iostream>
#include <string>

int
SuitIdx(char glyph)
{
  if (glyph == 'P')
    return 0;
  if (glyph == 'K')
    return 1;
  if (glyph == 'H')
    return 2;
  if (glyph == 'T')
    return 3;
  return 0;
}

int
main()
{
  bool cards[4][13] = {};
  std::string line;
  std::cin >> line;
  int n = line.size();
  for (int i = 0; i < n; i += 3) {
    int suit = SuitIdx(line[i]);
    int rank = std::stoi(line.substr(i + 1, 2)) - 1;
    if (cards[suit][rank]) {
      std::cout << "GRESKA" << std::endl;
      return 0;
    }
    cards[suit][rank] = true;
  }

  char const* sep = "";
  for (int i = 0; i < 4; ++i) {
    int n = std::count(std::begin(cards[i]), std::end(cards[i]), true);
    std::cout << sep << 13 - n;
    sep = " ";
  }
  std::cout << std::endl;
}
