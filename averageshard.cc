#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

int
main()
{
  int t = 0;
  std::cin >> t;
  for (int cse = 1; cse <= t; ++cse) {
    int ncs, nes;
    std::cin >> ncs >> nes;
    std::vector<unsigned> cs(ncs);
    std::copy_n(std::istream_iterator<unsigned>{ std::cin }, ncs, cs.begin());
    std::sort(cs.begin(), cs.end());
    unsigned csTot = std::accumulate(cs.begin(), cs.end(), 0);
    double oldCsAvg = (double)csTot / ncs;

    std::vector<unsigned> es(nes);
    std::copy_n(std::istream_iterator<unsigned>{ std::cin }, nes, es.begin());
    unsigned esTot = std::accumulate(es.begin(), es.end(), 0);
    double oldEsAvg = (double)esTot / nes;

    int tooCleverCs = 0;
    for (int i = 0; i < ncs; ++i) {
      int iq = cs[i];
      int newCsTot = csTot - iq;
      // int newEsTot = esTot + iq;
      double newCsAvg = (double)newCsTot / (ncs - 1);
      // double newEsAvg = (double)newEsTot / (nes + 1);
      // fprintf(stderr, "%lf %lf %lf %lf\n", oldCsAvg, newCsAvg, oldEsAvg,
      // newEsAvg);
      if (newCsAvg <= oldCsAvg) {
        tooCleverCs = i;
        break;
      }
    }
    int tooDumbCs = 0;
    for (int i = ncs - 1; i >= 0; --i) {
      int iq = cs[i];
      int newEsTot = esTot + iq;
      double newEsAvg = (double)newEsTot / (nes + 1);
      if (newEsAvg <= oldEsAvg) {
        tooDumbCs = i;
        break;
      }
    }
    std::cout << (std::max)(0, tooCleverCs - 1 - tooDumbCs) << std::endl;
#if 0
    int n =
      std::accumulate(cs.begin(), cs.end(), 0, [&](unsigned n, unsigned iq) {
        int newCsTot = csTot - iq;
        int newEsTot = esTot + iq;
        double newCsAvg = (double)newCsTot / (ncs - 1);
        double newEsAvg = (double)newEsTot / (nes + 1);
        if (newCsAvg > oldCsAvg && newEsAvg > oldEsAvg)
          ++n;
        return n;
      });
    std::cout << n << std::endl;
#endif
  }
}
