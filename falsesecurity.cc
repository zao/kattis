#include <algorithm>
#include <cstring>
#include <iostream>
#include <stack>
#include <string>

static char const* letters[] = { ".-",   "-...", "-.-.", "-..",  ".",    "..-.",
                                 "--.",  "....", "..",   ".---", "-.-",  ".-..",
                                 "--",   "-.",   "---",  ".--.", "--.-", ".-.",
                                 "...",  "-",    "..-",  "...-", ".--",  "-..-",
                                 "-.--", "--.." };

char const*
Encode(char ch)
{
  switch (ch) {
    case '_':
      return "..--";
    case ',':
      return ".-.-";
    case '.':
      return "---.";
    case '?':
      return "----";
    default:
      return letters[ch - 'A'];
  }
}

char
Decode(std::string doots)
{
  if (doots == "..--")
    return '_';
  if (doots == ".-.-")
    return ',';
  if (doots == "---.")
    return '.';
  if (doots == "----")
    return '?';
  auto I = std::find_if(std::begin(letters), std::end(letters),
                        [&](char const* s) { return doots == s; });
  return (char)(std::distance(std::begin(letters), I) + 'A');
}

int
main()
{
  std::string line;
  while (std::getline(std::cin, line)) {
    std::string doots;
    std::stack<int> lengths;
    for (auto ch : line) {
      char const* code = Encode(ch);
      doots += code;
      lengths.push(strlen(code));
    }
    char const* p = doots.c_str();
    while (!lengths.empty()) {
      auto len = lengths.top();
      char ch = Decode(std::string(p, p + len));
      std::cout << ch;
      p += len;
      lengths.pop();
    }
    std::cout << std::endl;
  }
}
