#include <iostream>

int
main()
{
  std::string note, tonality;
  int cse = 1;
  while (std::cin >> note >> tonality) {
    if (note.size() == 1) {
      printf("Case %d: UNIQUE\n", cse);
    } else {
      int idx = note[0] - 'A';
      char out[3] = {};
      if (note[1] == '#') {
        out[0] = 'A' + (idx + 1) % 7;
        out[1] = 'b';
      } else {
        out[0] = 'A' + (idx + 6) % 7;
        out[1] = '#';
      }
      printf("Case %d: %s %s\n", cse, out, tonality.c_str());
    }
    ++cse;
  }
}
