#include <cmath>
#include <iostream>
#include <vector>

struct vec2
{
  int x, y;
};

struct Rect
{
  vec2 p[2];
};

struct Circle
{
  vec2 c;
  int r;
};

int
main()
{
  int m;
  std::cin >> m;
  std::vector<Rect> rects;
  rects.reserve(30);
  std::vector<Circle> circles;
  circles.reserve(30);
  for (int i = 0; i < m; ++i) {
    std::string kind;
    std::cin >> kind;
    if (kind[0] == 'r') {
      Rect r;
      std::cin >> r.p[0].x >> r.p[0].y >> r.p[1].x >> r.p[1].y;
      rects.push_back(r);
    } else {
      Circle c;
      std::cin >> c.c.x >> c.c.y >> c.r;
      circles.push_back(c);
    }
  }
  int n;
  std::cin >> n;
  for (int i = 0; i < n; ++i) {
    vec2 pos;
    std::cin >> pos.x >> pos.y;
    int hits = 0;
    for (Rect const& r : rects) {
      if (pos.x >= r.p[0].x && pos.x <= r.p[1].x && pos.y >= r.p[0].y &&
          pos.y <= r.p[1].y) {
        ++hits;
      }
    }
    for (Circle const& c : circles) {
      vec2 rel = { pos.x - c.c.x, pos.y - c.c.y };
      if (rel.x * rel.x + rel.y * rel.y <= c.r * c.r) {
        ++hits;
      }
    }
    std::cout << hits << std::endl;
  }
}
