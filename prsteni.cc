#include <cmath>
#include <iostream>
#include <vector>

double const pi = 3.14159265359;

struct Rational
{
  int p, q;
};

int
GCD(int a, int b)
{
  if (b == 0)
    return a;
  return GCD(b, a % b);
}

Rational operator*(Rational a, Rational b)
{
  Rational ret = { a.p * b.p, a.q * b.q };
  int g = GCD(ret.p, ret.q);
  ret.p /= g;
  ret.q /= g;
  return ret;
}

Rational
operator/(Rational a, Rational b)
{
  std::swap(b.p, b.q);
  Rational ret = { a.p * b.p, a.q * b.q };
  int g = GCD(ret.p, ret.q);
  ret.p /= g;
  ret.q /= g;
  return ret;
}

int
main()
{
  int n;
  std::cin >> n;
  Rational prevFrac = { 1, 1 };
  int prevRad = 1;
  for (int i = 0; i < n; ++i) {
    int rad;
    std::cin >> rad;
    Rational r = { prevRad, rad };
    prevRad = rad;
    if (i >= 1) {
      Rational frac = r * prevFrac;
      prevFrac = frac;
      printf("%d/%d\n", frac.p, frac.q);
    }
  }
}
