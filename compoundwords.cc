#include <algorithm>
#include <iostream>
#include <iterator>
#include <set>
#include <sstream>
#include <string>

int
main()
{
  std::string line;
  std::set<std::string> fragments;
  std::copy(std::istream_iterator<std::string>(std::cin), {},
            std::inserter(fragments, fragments.end()));
  std::set<std::string> results;
  auto E = std::prev(fragments.end(), 1);
  for (auto I = fragments.begin(); I != E; ++I) {
    for (auto J = std::next(I); J != fragments.end(); ++J) {
      results.insert(*I + *J);
      results.insert(*J + *I);
    }
  }
  for (auto const& word : results) {
    std::cout << word << std::endl;
  }
}
