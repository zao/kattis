#include <cmath>
#include <iostream>

double const pi = 3.14159265358979323846;

double
CylVol(double r, double h)
{
  return pi * h * r * r;
}

double
ConeFrustum(double r1, double r2, double h)
{
  return (1.0 / 3.0) * pi * h * ((r1 * r1) + (r1 * r2) + (r2 * r2));
}

double
EatenVol(double D, double d)
{
  double R = D / 2.0;
  double r = d / 2.0;
  return CylVol(R, D) - CylVol(r, d) - 2.0 * ConeFrustum(R, r, R - r);
}

int
main()
{
  double d, v;
  while (1) {
    std::cin >> d >> v;
    if (d == 0 && v == 0)
      break;

    double left = 0.0;
    double right = d;

    while (1) {
      double mid = (left + right) / 2.0;
      double midVol = EatenVol(d, mid);
      if (midVol > v) {
        left = mid;
      } else {
        right = mid;
      }
      if (std::fabs(left - right) < 1e-10) {
        break;
      }
    }
    printf("%0.9f\n", (left + right) / 2.0);
  }
}
