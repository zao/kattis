#include <algorithm>
#include <iostream>
#include <vector>

int
main()
{
  int n;
  while (1) {
    scanf("%d", &n);
    if (!n)
      break;
    using Aug = std::pair<int, int>;
    std::vector<std::pair<int, int>> aAug(n);
    std::vector<int> b(n);
    for (int i = 0; i < n; ++i) {
      scanf("%d", &aAug[i].first);
      aAug[i].second = i;
    }
    for (int i = 0; i < n; ++i) {
      scanf("%d", &b[i]);
    }
    std::sort(std::begin(b), std::end(b));
    std::sort(std::begin(aAug), std::end(aAug),
              [](Aug lhs, Aug rhs) { return lhs.first < rhs.first; });

    std::vector<int> bCorr(n);
    for (int i = 0; i < n; ++i) {
      int dst = aAug[i].second;
      bCorr[dst] = b[i];
    }
    for (auto v : bCorr) {
      printf("%d\n", v);
    }
    printf("\n");
  }
}
