#include <algorithm>
#include <iostream>
#include <iterator>
#include <map>
#include <set>
#include <vector>

int
main()
{
  int w, p;
  std::cin >> w >> p;
  std::vector<int> locs;
  locs.reserve(p + 2);
  locs.push_back(0);
  std::copy_n(std::istream_iterator<int>(std::cin), p,
              std::back_inserter(locs));
  locs.push_back(w);
  std::set<int> widths;
  int k = locs.size();
  for (int left = 0; left < k - 1; ++left) {
    for (int right = left + 1; right < k; ++right) {
      widths.insert(locs[right] - locs[left]);
    }
  }
  char const* sep = "";
  for (auto width : widths) {
    std::cout << sep << width;
    sep = " ";
  }
  std::cout << std::endl;
}
