#include <iostream>

int
main()
{
  int t;
  std::cin >> t;
  for (int cse = 0; cse < t; ++cse) {
    int n;
    std::cin >> n;
    int lo = 99, hi = 0;
    for (int i = 0; i < n; ++i) {
      int x;
      std::cin >> x;
      if (lo > x)
        lo = x;
      if (hi < x)
        hi = x;
    }
    int cost = 2 * (hi - lo);
    std::cout << cost << std::endl;
  }
}
