#include <iostream>
#include <string>

int
main()
{
  int n;
  std::cin >> n;
  for (int cse = 0; cse < n; ++cse) {
    std::string tok;
    std::cin >> tok;
    std::cout << tok.size() << std::endl;
  }
}
