#include <algorithm>
#include <cmath>
#include <iostream>
#include <map>
#include <string>
#include <vector>

int
main()
{
  int n;
  std::cin >> n;
  for (int cse = 0; cse < n; ++cse) {
    int vals[1001];
    int m;
    std::cin >> m;
    int lo = +1000000;
    int hi = -1000000;
    for (int i = 0; i < m; ++i) {
      int val;
      std::cin >> val;
      vals[i] = val;
      if (lo > val)
        lo = val;
      if (hi < val)
        hi = val;
    }
    bool arith = true;
    bool sorted = (vals[0] < vals[1])
                    ? std::is_sorted(vals, vals + m)
                    : std::is_sorted(vals, vals + m, std::greater<int>());
    std::sort(vals, vals + m);
    int del = vals[1] - vals[0];
    for (int i = 1; i < m - 1; ++i) {
      if (vals[i + 1] - vals[i] != del) {
        arith = false;
        break;
      }
    }
    if (!arith) {
      std::cout << "non-arithmetic" << std::endl;
    } else if (sorted) {
      std::cout << "arithmetic" << std::endl;
    } else {
      std::cout << "permuted arithmetic" << std::endl;
    }
  }
}
