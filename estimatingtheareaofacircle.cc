#include <iostream>

int
main()
{
  double const pi = 3.14159265358979323846;
  while (1) {
    double r, m, c;
    std::cin >> r >> m >> c;
    if (r == 0 && m == 0 && c == 0)
      return 0;
    printf("%0.9f %0.9f\n", pi * r * r, 4.0 * r * r * c / m);
  }
}
