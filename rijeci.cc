#include <iostream>

int
main()
{
  int as = 1, bs = 0;
  int k;
  std::cin >> k;
  for (int i = 0; i < k; ++i) {
    int bs2 = bs + as;
    int as2 = bs;
    bs = bs2;
    as = as2;
  }
  std::cout << as << " " << bs << std::endl;
}
