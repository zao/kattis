#include <algorithm>
#include <iostream>
#include <iterator>
#include <map>
#include <queue>
#include <set>
#include <vector>

int
main()
{
  int n, m;
  std::cin >> n >> m;
  std::vector<int> balances(n);
  std::multimap<int, int> friendEdges;
  std::copy_n(std::istream_iterator<int>(std::cin), n, balances.begin());
  for (int i = 0; i < m; ++i) {
    int a, b;
    std::cin >> a >> b;
    friendEdges.emplace(a, b);
    friendEdges.emplace(b, a);
  }
  std::map<int, int> treeBalances;
  std::vector<int> treeIds(n);
  std::set<int> uncheckedNodes;
  for (int i = 0; i < n; ++i) {
    uncheckedNodes.insert(i);
  }
  int nextFreeId = 1;
  while (!uncheckedNodes.empty()) {
    int current = *uncheckedNodes.begin();
    int treeId = nextFreeId++;
    std::queue<int> wavefront;
    wavefront.push(current);
    do {
      int i = wavefront.front();
      wavefront.pop();
      uncheckedNodes.erase(i);
      if (treeIds[i] == 0) {
        treeIds[i] = treeId;
        treeBalances[treeId] += balances[i];
        for (auto rng = friendEdges.equal_range(i); rng.first != rng.second;
             ++rng.first) {
          int dst = rng.first->second;
          wavefront.push(dst);
        }
      }
    } while (!wavefront.empty());
  }
  for (auto I = treeBalances.begin(); I != treeBalances.end(); ++I) {
    if (I->second) {
      std::cout << "IMPOSSIBLE" << std::endl;
      return 0;
    }
  }
  std::cout << "POSSIBLE" << std::endl;
}
