#include <algorithm>
#include <iostream>
#include <vector>

struct Range
{
  int from, to;
  int count;
};

bool
Overlaps(Range l, Range r)
{
  if (l.to <= r.from || l.from >= r.to)
    return false;
  return true;
}

int
main()
{
  char counts[101] = {};
  int a, b, c;
  std::cin >> a >> b >> c;
  std::vector<Range> ranges;
  for (int i = 0; i < 3; ++i) {
    Range r;
    r.count = 1;
    std::cin >> r.from >> r.to;
    for (int j = r.from; j < r.to; ++j) {
      counts[j]++;
    }
    ranges.push_back(r);
  }
  std::sort(ranges.begin(), ranges.end(),
            [](Range l, Range r) { return l.from < r.from; });
  int prices[] = { 0, a, b, c };
  int sum = 0;
  for (auto c : counts) {
    sum += c * prices[c];
  }
  std::cout << sum << std::endl;
}
