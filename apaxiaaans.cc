#include <iostream>
#include <string>

int
main()
{
  std::string name;
  std::cin >> name;
  char last = '\0';
  int n = name.size();
  for (char ch : name) {
    if (ch != last) {
      std::cout << ch;
    }
    last = ch;
  }
  std::cout << std::endl;
}
