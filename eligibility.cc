#include <iostream>
#include <string>

struct Date
{
  Date() {}
  Date(std::string str)
    : year(std::atoi(str.substr(0, 4).c_str()))
  {
  }

  int year;
};

int
main()
{
  int n;
  std::cin >> n;
  for (; n; --n) {
    std::string name;
    std::string ps, bd;
    int courses;
    std::cin >> name >> ps >> bd >> courses;
    Date postSec(ps);
    Date birthDate(bd);
    std::cout << name;
    if (postSec.year >= 2010 || birthDate.year >= 1991) {
      std::cout << " eligible" << std::endl;
    } else if (courses / 5.0f > 8.0f) {
      std::cout << " ineligible" << std::endl;
    } else {
      std::cout << " coach petitions" << std::endl;
    }
  }
}
