#include <algorithm>
#include <iostream>

int
main()
{
  int f[4];
  std::cin >> f[0] >> f[1] >> f[2] >> f[3];
  std::sort(std::begin(f), std::end(f));
  int bestArea = 0;
  do {
    if (f[0] < f[2] || f[1] > f[3]) {
      continue;
    }
    int w = f[2];
    int h = f[1];
    int area = w * h;
    if (bestArea < area) {
      bestArea = area;
    }
  } while (std::next_permutation(std::begin(f), std::end(f)));
  std::cout << bestArea << std::endl;
}
