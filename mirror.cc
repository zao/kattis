#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

int
main()
{
  int t;
  std::cin >> t;
  for (int cse = 1; cse <= t; ++cse) {
    int r, c;
    std::cin >> r >> c;
    std::vector<char> v;
    v.reserve(r * c);
    std::string line;
    for (int row = 0; row < r; ++row) {
      std::cin >> line;
      std::copy(line.begin(), line.end(), std::back_inserter(v));
    }
    std::cout << "Test " << cse << std::endl;
    for (int i = 0; i < r * c; ++i) {
      std::cout << v[r * c - i - 1];
      if (i % c == (c - 1))
        std::cout << std::endl;
    }
    std::cout << std::endl;
  }
}
