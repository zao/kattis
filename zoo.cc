#include <iostream>
#include <map>
#include <string>

int
main()
{
  int cse = 1;
  int n;
  while (std::cin >> n && n) {
    std::cin.ignore(5, '\n');
    std::map<std::string, int> tally;
    for (int i = 0; i < n; ++i) {
      std::string line;
      std::getline(std::cin, line);
      auto off = line.find_last_of(' ');
      if (off == std::string::npos) {
        off = -1;
      }
      std::string name = line.substr(off + 1);
      for (auto& ch : name) {
        if (ch >= 'A' && ch <= 'Z') {
          ch = ch - 'A' + 'a';
        }
      }
      ++tally[name];
    }
    printf("List %d:\n", cse);
    for (auto const& p : tally) {
      printf("%s | %d\n", p.first.c_str(), p.second);
    }
    ++cse;
  }
}
