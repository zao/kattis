#include <algorithm>
#include <iostream>
#include <iterator>
#include <set>
#include <sstream>
#include <string>
#include <vector>

int
main()
{
  int t;
  std::cin >> t;
  std::cin.ignore(5, '\n');
  for (int cse = 1; cse <= t; ++cse) {
    std::string line;
    using Sound = std::string;
    std::vector<Sound> recording;
    {
      std::getline(std::cin, line);
      std::istringstream iss(line);
      std::copy(std::istream_iterator<std::string>(iss), {},
                std::back_inserter(recording));
    }
    std::set<Sound> others;
    {
      while (std::getline(std::cin, line) && line != "what does the fox say?") {
        std::istringstream iss(line);
        std::string _, sound;
        iss >> _ >> _ >> sound;
        others.insert(sound);
      }
    }
    char const* sep = "";
    for (auto word : recording) {
      if (!others.count(word)) {
        std::cout << sep << word;
        sep = " ";
      }
    }
    std::cout << std::endl;
  }
}
