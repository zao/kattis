#include <iostream>

int
main()
{
  int w, l;
  while (std::cin >> w >> l && w && l) {
    int n;
    std::cin >> n;
    int ax = 0, ay = 0;
    int rx = 0, ry = 0;
    for (int i = 0; i < n; ++i) {
      char dir;
      int amt;
      std::cin >> dir >> amt;
      switch (dir) {
        case 'd':
          ay -= amt;
          if (ay < 0)
            ay = 0;
          ry -= amt;
          break;
        case 'u':
          ay += amt;
          if (ay >= l)
            ay = l - 1;
          ry += amt;
          break;
        case 'l':
          ax -= amt;
          if (ax < 0)
            ax = 0;
          rx -= amt;
          break;
        case 'r':
          ax += amt;
          if (ax >= w)
            ax = w - 1;
          rx += amt;
          break;
      }
    }
    printf("Robot thinks %d %d\n", rx, ry);
    printf("Actually at %d %d\n", ax, ay);
    printf("\n");
  }
}
