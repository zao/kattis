#include <iostream>

char const* news[] = { "@",   "8",      "(",   "|)", "3", "#",       "6",
                       "[-]", "|",      "_|",  "|<", "1", "[]\\/[]", "[]\\[]",
                       "0",   "|D",     "(,)", "|Z", "$", "']['",    "|_|",
                       "\\/", "\\/\\/", "}{",  "`/", "2" };

int
main()
{
  std::string line;
  std::getline(std::cin, line);
  for (auto ch : line) {
    if (ch >= 'A' && ch <= 'Z')
      std::cout << news[ch - 'A'];
    else if (ch >= 'a' && ch <= 'z')
      std::cout << news[ch - 'a'];
    else
      std::cout << ch;
  }
  std::cout << std::endl;
}
