#include <iostream>
#include <map>
#include <string>

int
main()
{
  std::map<int, std::string> cups;
  int n;
  std::cin >> n;
  for (int i = 0; i < n; ++i) {
    std::string w;
    std::cin >> w;
    int r = std::atoi(w.c_str());
    if (r) {
      std::cin >> w;
      cups[r/2] = w;
    }
    else {
      std::cin >> r;
      cups[r] = w;
    }
  }
  for (auto p : cups) {
    std::cout << p.second << std::endl;
  }
}
