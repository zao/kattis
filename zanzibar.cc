#include <iostream>

int main() {
  int t;
  std::cin >> t;
  for (; t; --t) {
    int prevGen = 1;
    int immigrants = 0;
    int count;
    while (std::cin >> count && count) {
      int newbies = count - prevGen;
      if (newbies > prevGen) {
        immigrants += newbies - prevGen;
      }
      prevGen = count;
    }
    std::cout << immigrants << std::endl;
  }
}
