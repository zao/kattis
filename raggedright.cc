#include <iostream>
#include <string>
#include <vector>

int
main()
{
  std::vector<int> lens;
  int longestLine = 0;
  std::string line;
  while (std::getline(std::cin, line)) {
    int len = line.size();
    if (len == 0)
      continue;
    lens.push_back(len);
    if (longestLine < len)
      longestLine = len;
  }
  int penaltySum = 0;
  for (int i = 0; i < lens.size() - 1; ++i) { // discard last line
    int penalty = (longestLine - lens[i]);
    penaltySum += penalty * penalty;
  }
  std::cout << penaltySum << std::endl;
}
