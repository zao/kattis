#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

int
main()
{
  int t = 0;
  std::cin >> t;
  for (int cse = 1; cse <= t; ++cse) {
    int ncs, nes;
    std::cin >> ncs >> nes;
    std::vector<unsigned> cs(ncs);
    std::copy_n(std::istream_iterator<unsigned>{ std::cin }, ncs, cs.begin());
    std::sort(cs.begin(), cs.end());
    unsigned csTot = std::accumulate(cs.begin(), cs.end(), 0);
    double oldCsAvg = (double)csTot / ncs;

    std::vector<unsigned> es(nes);
    std::copy_n(std::istream_iterator<unsigned>{ std::cin }, nes, es.begin());
    unsigned esTot = std::accumulate(es.begin(), es.end(), 0);
    double oldEsAvg = (double)esTot / nes;

    int n =
      std::accumulate(cs.begin(), cs.end(), 0, [&](unsigned n, unsigned iq) {
        int newCsTot = csTot - iq;
        int newEsTot = esTot + iq;
        double newCsAvg = (double)newCsTot / (ncs - 1);
        double newEsAvg = (double)newEsTot / (nes + 1);
        if (newCsAvg > oldCsAvg && newEsAvg > oldEsAvg)
          ++n;
        return n;
      });
    std::cout << n << std::endl;
  }
}
