#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <sstream>
#include <string>

int
main()
{
  std::string line;
  while (std::getline(std::cin, line)) {
    std::istringstream iss(line);
    int sum = std::accumulate(std::istream_iterator<int>(iss), {}, 0);
    std::cout << (sum / 2) << std::endl;
  }
}
