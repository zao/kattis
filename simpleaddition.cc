#include <algorithm>
#include <iostream>

int
main()
{
  std::string s0, s1;
  std::cin >> s0 >> s1;
  if (s0.size() > s1.size())
    std::swap(s0, s1);
  int n0 = s0.size();
  int n1 = s1.size();

  std::string sum(n1, '0');
  int carry = 0;
  std::transform(s0.rbegin(), s0.rend(), s1.rbegin(), sum.rbegin(),
                 [&carry](char a, char b) {
                   int x = a - '0';
                   int y = b - '0';
                   int sum = x + y + carry;
                   carry = sum / 10;
                   return (sum % 10) + '0';
                 });
  std::transform(s1.rbegin() + n0, s1.rend(), sum.rbegin() + n0,
                 [&carry](char a) {
                   int x = a - '0';
                   int sum = x + carry;
                   carry = sum / 10;
                   return (sum % 10) + '0';
                 });
  if (carry) {
    std::cout << carry;
  }
  std::cout << sum << std::endl;
}
