#include <cmath>
#include <iostream>

double const pi = 3.14159265359;

int
main()
{
  int n;
  std::cin >> n;
  for (int cse = 1; cse <= n; ++cse) {
    int m;
    std::cin >> m;
    double x = 0.0, y = 0.0;
    double dir = 90.0;
    for (int i = 0; i < m; ++i) {
      double ang, mag;
      std::cin >> ang >> mag;
      dir += ang;
      double dx = mag * cos(pi * dir / 180.0);
      double dy = mag * sin(pi * dir / 180.0);
      x += dx;
      y += dy;
      fprintf(stderr, "| x=%0.9f y=%0.9f dir=%0.9f\n", x, y, dir);
    }
    printf("%0.9f %0.9f\n", x, y);
  }
}
