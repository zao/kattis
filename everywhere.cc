#include <iostream>
#include <map>
#include <set>
#include <string>

int main() {
  int nTrips;
  std::cin >> nTrips;
  for (int trip = 0; trip < nTrips; ++trip) {
    int nCities;
    std::cin >> nCities;
    std::set<std::string> names;
    for (int city = 0; city < nCities; ++city) {
      std::string name;
      std::cin >> name;
      names.insert(name);
    }
    std::cout << names.size() << std::endl;
  }
}
