#include <cmath>
#include <iostream>

int
main()
{
  double x1, y1, x2, y2, p;
  while (std::cin >> x1 && x1) {
    std::cin >> y1 >> x2 >> y2 >> p;
    double norm = std::pow(
      std::pow(std::abs(x1 - x2), p) + std::pow(std::abs(y1 - y2), p), 1.0 / p);
    printf("%0.9f\n", norm);
  }
}
