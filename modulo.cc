#include <iostream>
#include <set>

int main() {
  std::set<int> rems;
  for (int i = 0; i < 10; ++i) {
    int val;
    std::cin >> val;
    rems.insert(val % 42);
  }
  std::cout << rems.size() << std::endl;
}
