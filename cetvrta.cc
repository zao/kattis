#include <algorithm>
#include <iostream>
#include <set>

int main() {
  int xs[3] = {}, ys[3] = {};
  for (int i = 0; i < 3; ++i) {
    std::cin >> xs[i] >> ys[i];
  }
  int x, y;
  if (xs[0] == xs[1]) x = xs[2];
  else if (xs[0] == xs[2]) x = xs[1];
  else x = xs[0];
  if (ys[0] == ys[1]) y = ys[2];
  else if (ys[0] == ys[2]) y = ys[1];
  else y = ys[0];
  std::cout << x << " " << y << std::endl;
}
