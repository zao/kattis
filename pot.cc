#include <cmath>
#include <iostream>

int main() {
  int n;
  std::cin >> n;
  int sum = 0;
  for (int i = 0; i < n; ++i) {
    int p;
    std::cin >> p;
    int exponent = p % 10;
    p /= 10;
    sum += (int)std::pow(p, exponent);
  }
  std::cout << sum << std::endl;
}
