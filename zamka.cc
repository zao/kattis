#include <iostream>

int
main()
{
  int l, d, x;
  std::cin >> l >> d >> x;
  int n;
  for (int i = l; i <= d; ++i) {
    int val = i;
    int sum = 0;
    while (val) {
      sum += val % 10;
      val /= 10;
    }
    if (sum == x) {
      n = i;
      break;
    }
  }
  int m;
  for (int i = d; i >= n; --i) {
    int val = i;
    int sum = 0;
    while (val) {
      sum += val % 10;
      val /= 10;
    }
    if (sum == x) {
      m = i;
      break;
    }
  }
  std::cout << n << std::endl << m << std::endl;
}
