#include <iostream>

int
main()
{
  int m, p, l, e, r, s, n;
  while (std::cin >> m >> p >> l >> e >> r >> s >> n) {
    for (int i = 0; i < n; ++i) {
      int lNew = m * e;
      int pNew = l / r;
      int mNew = p / s;
      l = lNew;
      p = pNew;
      m = mNew;
    }
    std::cout << m << std::endl;
  }
}
