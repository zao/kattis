#include <cmath>
#include <iostream>
#include <string>

int
main()
{
  int n;
  std::cin >> n;
  for (int cse = 0; cse < n; ++cse) {
    std::string msg;
    std::cin >> msg;
    int side = (int)std::sqrt(msg.size());
    for (int col = side - 1; col >= 0; --col) {
      for (int row = 0; row < side; ++row) {
        std::cout << msg[row * side + col];
      }
    }
    std::cout << std::endl;
  }
}
