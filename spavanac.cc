#include <iostream>

int
main()
{
  int h, m;
  std::cin >> h >> m;
  if (m < 45) {
    h = (h - 1 + 24) % 24;
  }
  m = (m - 45 + 60) % 60;
  std::cout << h << " " << m << std::endl;
}
