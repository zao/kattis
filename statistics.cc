#include <algorithm>
#include <iostream>

int
main()
{
  int cse = 1;
  int n;
  while (std::cin >> n) {
    int lo = +1000000, hi = -1000000;
    for (int i = 0; i < n; ++i) {
      int v;
      std::cin >> v;
      if (lo > v)
        lo = v;
      if (hi < v)
        hi = v;
    }
    printf("Case %d: %d %d %d\n", cse, lo, hi, hi - lo);
    ++cse;
  }
}
