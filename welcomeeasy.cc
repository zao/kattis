#include <algorithm>
#include <array>
#include <iostream>
#include <iterator>
#include <map>
#include <string>
#include <vector>

int
main()
{
  std::string msg = "welcome to code jam";
  int const nch = msg.size();

  int t;
  std::cin >> t;
  std::cin.ignore(100, '\n');
  for (int cse = 0; cse < t; ++cse) {
    std::string haystack;
    std::getline(std::cin, haystack);
    while (std::isspace(haystack.back())) { // strip trailing whitespace like \r
      haystack.pop_back();
    }
    int len = haystack.size();

    // intentionally accessing \0 on [0]
    int m = haystack.size();
    int n = msg.size();
    auto x = [&haystack, m](int i) { return haystack[m - i]; };
    auto y = [&msg, n](int i) { return msg[n - i]; };

    int const tableCols = m + 1;
    int const tableRows = n + 1;

    std::vector<int> grid((m + 1) * (n + 1), 0);
    auto c = [&grid, tableCols](int x, int y) -> int& {
      return grid.at(x + tableCols * y);
    };

    for (int col = 0; col < tableCols; ++col) {
      c(col, 0) = 1;
    }
    for (int row = 1; row < tableRows; ++row) {
      c(0, row) = 0;
    }

    for (int row = 1; row < tableRows; ++row) {
      for (int col = 1; col < tableCols; ++col) {
        int& cell = c(col, row);
        if (x(col) == y(row)) {
          cell = c(col - 1, row) + c(col - 1, row - 1);
        } else {
          cell = c(col - 1, row);
        }
        cell %= 10000;
      }
    }

    int matches = c(m, n);
    printf("Case #%d: %04d\n", cse + 1, matches);
  }
}
