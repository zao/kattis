#include <cmath>
#include <cstdio>
#include <iostream>

int
main()
{
  int r;
  std::cin >> r;
  double pi = 2.0 * std::asin(1);
  double euArea = pi * r * r;
  double tcArea = 2.0 * r * r;
  printf("%0.9f\n%0.9f\n", euArea, tcArea);
}
