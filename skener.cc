#include <iostream>
#include <string>

int
main()
{
  int r, c, zr, zc;
  std::cin >> r >> c >> zr >> zc;
  std::string line;
  for (int row = 0; row < r; ++row) {
    std::cin >> line;
    for (int zrow = 0; zrow < zr; ++zrow) {
      for (int col = 0; col < c; ++col) {
        char ch = line[col];
        for (int zcol = 0; zcol < zc; ++zcol) {
          std::cout << ch;
        }
      }
      std::cout << std::endl;
    }
  }
}
