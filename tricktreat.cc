#include <algorithm>
#include <cfloat>
#include <cmath>
#include <iostream>
#include <sstream>
#include <vector>

struct Point
{
  double x, y;
};

double
DistanceSquared(double x, double y, double t)
{
  return y * y + (x - t) * (x - t);
}

struct Top {
  double y2;
};

Top
FindTop(Point* points, int n, double t)
{
  Top top = { -FLT_MAX };
  for (int i = 0; i < n; ++i) {
    double y2 = DistanceSquared(points[i].x, points[i].y, t);
    if (top.y2 <= y2) {
      top.y2 = y2;
    }
  }
  return top;
}

void
Case(std::istream& is, std::ostream& os, int n, int caseIdx)
{
  char buf[512] = {};
  std::vector<Point> points(n);

  std::cerr << "=== CASE ===\n";

  double minX = +FLT_MAX, maxX = -FLT_MAX;
  for (int i = 0; i < n; ++i) {
    double x, y;
    is >> x >> y;
    if (minX > x) {
      minX = x;
    }
    if (maxX < x) {
      maxX = x;
    }
    points[i].x = x;
    points[i].y = y;
  }

  double lowT = minX, highT = maxX;
  double curT;
  double curY2 = +FLT_MAX;
  do {
    double w = (highT - lowT) / 3.0f;
    double m1T = lowT + w;
    double m2T = highT - w;
    double m1Y2 = FindTop(&points[0], n, m1T).y2;
    double m2Y2 = FindTop(&points[0], n, m2T).y2;
    bool goodEnough = (fabs(m2Y2 - curY2) < 1e-11 && fabs(m1Y2 - curY2) < 1e-11);
    if (m1Y2 < m2Y2) {
      // discard right chunk
      highT = m2T;
      curT = m1T;
      curY2 = m1Y2;
    }
    else {
      // discard left chunk
      lowT = m1T;
      curT = m2T;
      curY2 = m2Y2;
    }
    sprintf(buf, "%0.10f, %0.10f", lowT, highT);
    std::cerr << buf << std::endl;
    if (goodEnough) {
      break;
    }
  } while (highT - lowT > 1e-7);

  sprintf(buf, "%0.10f, %0.10f", curT, sqrt(curY2));
  os << buf << std::endl;
}

void
Run(std::istream& is, std::ostream& os)
{
  int caseIdx = 0;
  int n;
  while (is >> n) {
    if (n == 0) {
      return;
    }

    Case(is, os, n, caseIdx++);
  }
}

#define RUN 0

#if RUN == 1
#include <random>
#endif

int
main()
{
#if RUN == 0
  Run(std::cin, std::cout);
#elif RUN == 1
  std::mt19937 rng;
  std::uniform_real_distribution<float> dist(-200'000.0f, +200'000.0f);
  std::stringstream ss;
  int n = 50'000;
  ss << n << "\n";
  for (int i = 0; i < n; ++i) {
    float x = dist(rng), y = dist(rng);
    ss << x << " " << y << "\n";
  }
  ss << 0 << std::endl;
  Run(ss, std::cout);
#else
  char const* sampleInput = "2\n"
    "1.5 1.5\n"
    "3 0\n"
    "\n"
    "1\n"
    "0 0\n"
    "\n"
    "4\n"
    "1 4\n"
    "4 4\n"
    "-3 3\n"
    "2 4\n"
    "\n"
    "5\n"
    "4 7\n"
    "-4 0\n"
    "7 -6\n"
    "-2 4\n"
    "8 -5\n"
    "\n"
    "0\n";

  char const* sampleOutput = "1.500000000000 1.500000000000\n"
    "0.000000000000 0.000000000000\n"
    "1.000000000000 5.000000000000\n"
    "3.136363636363 7.136363636364\n";

  Run(std::istringstream(sampleInput), std::cout);
#endif
}
