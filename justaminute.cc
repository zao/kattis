#include <cmath>
#include <iostream>

int
main()
{
  int n;
  std::cin >> n;
  int totMins = 0, totSecs = 0;
  for (int i = 0; i < n; ++i) {
    int min, sec;
    std::cin >> min >> sec;
    totMins += min;
    totSecs += sec;
  }
  if (totSecs > totMins * 60) {
    printf("%0.9f\n", (double)(totSecs) / (totMins * 60.0));
  } else {
    printf("measurement error\n");
  }
}
