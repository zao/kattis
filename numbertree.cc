#include <iostream>

int
main()
{
  int h;
  std::cin >> h;
  std::string chain;
  std::cin >> chain;
  int n = chain.size();
  int root = (2 << h) - 1;
  int id = 1;
  for (auto ch : chain) {
    id <<= 1;
    id |= (ch == 'R');
  }
  printf("%d\n", root - id + 1);
}
