#include <iostream>

int
main()
{
  int cur, dst;
  std::cin >> cur >> dst;
  dst -= cur;
  if (dst <= -180)
    dst += 360;
  else if (dst > 180)
    dst -= 360;
  std::cout << dst << std::endl;
}
