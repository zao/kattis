#include <iostream>

int
main()
{
  int sweet, sour;
  while (std::cin >> sweet >> sour) {
    if (sweet == 0 && sour == 0)
      break;
    if (sweet + sour == 13)
      std::cout << "Never speak again.\n";
    else if (sweet < sour)
      std::cout << "Left beehind.\n";
    else if (sweet > sour)
      std::cout << "To the convention.\n";
    else
      std::cout << "Undecided.\n";
  }
}
