#include <iostream>
#include <limits>
#include <sstream>
#include <string>

std::string simonSays = "Simon says ";

int
main()
{
  int n;
  std::cin >> n;
  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  for (int cse = 0; cse < n; ++cse) {
    std::string line;
    std::getline(std::cin, line);
    if (line.substr(0, simonSays.size()) == simonSays) {
      std::cout << line.substr(simonSays.size()) << std::endl;
    }
  }
}
