#include <math.h>
#include <iostream>

double const pi = 3.14159265359;
double const g = 9.82;

int
main()
{
  int n;
  scanf("%d", &n);
  for (int cse = 0; cse < n; ++cse) {
    double v0, theta, x1, h1, h2;
    scanf("%lf %lf %lf %lf %lf", &v0, &theta, &x1, &h1, &h2);
    theta *= pi / 180.0;
    double ct = cos(theta);
    double st = sin(theta);
    double t1 = x1 / (v0 * ct);
    double y1 = (x1 * st) / ct - 0.5 * g * (x1 / (v0 * ct)) * (x1 / (v0 * ct));
    if (y1 - 1.0 > h1 && y1 + 1.0 < h2)
      printf("Safe\n");
    else
      printf("Not Safe\n");
  }
}
