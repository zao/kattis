#include <iostream>
#include <string>

int
main()
{
  int n;
  std::cin >> n;
  std::cin.ignore(5, '\n');
  std::string a, b;
  std::cin >> a >> b;
  bool good;
  if (n % 2) {
    good = true;
    int len = a.size();
    for (int i = 0; i < len; ++i) {
      if (a[i] == b[i]) {
        good = false;
        break;
      }
    }
  } else {
    good = a == b;
  }
  std::cout << (good ? "Deletion succeeded" : "Deletion failed") << std::endl;
}
