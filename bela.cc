#include <iostream>
#include <string>

int
main()
{
  int domRanks[] = { 0, 0, 14, 10, 20, 3, 4, 11 };
  int subRanks[] = { 0, 0, 0, 10, 2, 3, 4, 11 };

  int hands;
  char domSuit;
  std::cin >> hands >> domSuit;

  int sum = 0;
  for (int i = 0; i < hands * 4; ++i) {
    std::string card;
    std::cin >> card;
    int cIdx;
    switch (card[0]) {
      case '7':
        cIdx = 0;
        break;
      case '8':
        cIdx = 1;
        break;
      case '9':
        cIdx = 2;
        break;
      case 'T':
        cIdx = 3;
        break;
      case 'J':
        cIdx = 4;
        break;
      case 'Q':
        cIdx = 5;
        break;
      case 'K':
        cIdx = 6;
        break;
      case 'A':
        cIdx = 7;
        break;
    }
    if (card[1] == domSuit) {
      sum += domRanks[cIdx];
    } else {
      sum += subRanks[cIdx];
    }
  }
  std::cout << sum << std::endl;
}
