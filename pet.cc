#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>

int
main()
{
  int bestEntry = -1;
  int bestScore = -1;
  for (int row = 0; row < 5; ++row) {
    int grades[4];
    std::copy_n(std::istream_iterator<int>(std::cin), 4, std::begin(grades));
    int sum = std::accumulate(std::begin(grades), std::end(grades), 0);
    if (bestScore < sum) {
      bestScore = sum;
      bestEntry = row;
    }
  }
  std::cout << (bestEntry + 1) << " " << bestScore << std::endl;
}
