#include <iostream>

int
main()
{
  int k; // initial boxkeeper
  scanf("%d\n", &k);
  k -= 1;
  int n; // number of events
  scanf("%d\n", &n);
  int const limit = 3 * 60 + 30;
  int now = 0;
  for (int event = 0; event < n; ++event) {
    int t;  // time to answer the question
    char z; // answer
    scanf("%d %c\n", &t, &z);
    if (now + t >= limit) {
      break;
    } else {
      now += t;
      if (z == 'T') {
        k = (k + 1) % 8;
      }
    }
  }
  printf("%d\n", k + 1);
}
