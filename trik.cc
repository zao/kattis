#include <iostream>
#include <string>

int
main()
{
  std::string line;
  std::cin >> line;
  int ball = 0;
  for (char ch : line) {
    if (ch == 'A' && ball != 2) {
      ball = (ball == 0) ? 1 : 0;
    }
    else if (ch == 'B' && ball != 0) {
      ball = (ball == 2) ? 1 : 2;
    }
    else if (ch == 'C' && ball != 1) {
      ball = (ball == 0) ? 2 : 0;
    }
  }
  std::cout << (ball+1) << std::endl;
}
