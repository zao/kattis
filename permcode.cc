#include <algorithm>
#include <cmath>
#include <iostream>
#include <map>
#include <string>
#include <vector>

std::string
Encode(int x, std::string const& s, std::string const& p, std::string const& m)
{
  int n = m.size();
  std::string c(n, '#');
  int d = (int)(std::pow(n, 1.5) + x) % n;
  fprintf(stderr, "d=%d, n=%d\n", d, n);
  c[d] = s[p.find_first_of(m[d])];
  for (int j = 0; j < n; ++j) {
    if (j != d) {
      int pos = p.find_first_of(m[j]) ^ s.find_first_of(m[(j + 1) % n]);
      c[j] = s[pos];
    }
  }
  return c;
}

std::string
Decode(int x, std::string s, std::string p, std::string c)
{
  int n = c.size();
  std::string m(n, '#');
  int d = (int)(std::pow(n, 1.5) + x) % n;
  m[d] = p[s.find_first_of(c[d])];
  for (int i = 0; i < n - 1; ++i) {
    int j = (d + n - 1 - i) % n;
    int k = s.find_first_of(c[j]);
    int l = s.find_first_of(m[(j + 1) % n]);
    int a = k ^ l;
    m[j] = p[a];
  }
  return m;
}

int
main()
{
  int x;
  while (std::cin >> x && x) {
    std::string s, p, c;
    std::cin >> s;
    std::cin >> p;
    std::cin >> c;
    std::string m = Decode(x, s, p, c);
    std::cout << m << std::endl;
  }
}
