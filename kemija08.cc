#include <iostream>
#include <string>

int
main()
{
  std::string line;
  std::getline(std::cin, line);
  int n = line.size();
  for (int i = 0; i < n;) {
    char ch = line[i];
    std::cout << ch;
    if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
      i += 3;
    } else {
      ++i;
    }
  }
  std::cout << std::endl;
}
