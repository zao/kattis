#include <iostream>
#include <string>

int
main()
{
  int n;
  std::cin >> n;
  std::string lastName;
  std::cin >> lastName;
  bool increasing = false;
  bool decreasing = false;
  for (int i = 0; i < n - 1; ++i) {
    std::string name;
    std::cin >> name;
    if (lastName < name) {
      increasing = true;
    }
    if (lastName > name) {
      decreasing = true;
    }
    if (increasing && decreasing) {
      std::cout << "NEITHER" << std::endl;
      return 0;
    }
    lastName = name;
  }
  if (increasing) {
    std::cout << "INCREASING" << std::endl;
  }
  if (decreasing) {
    std::cout << "DECREASING" << std::endl;
  }
}
