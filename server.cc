#include <iostream>

int
main()
{
  int n, t;
  std::cin >> n >> t;
  int elapsed = 0;
  int completed = 0;
  for (int i = 0; i < n; ++i) {
    int duration;
    std::cin >> duration;
    if (elapsed + duration <= t) {
      elapsed += duration;
      completed++;
    } else {
      break;
    }
  }
  std::cout << completed << std::endl;
}
