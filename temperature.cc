#include <iostream>

int
main()
{
  int x, y;
  std::cin >> x >> y;
  int den = 1 - y;
  if (den == 0) {
    if (x == 0) {
      std::cout << "ALL GOOD" << std::endl;
    } else {
      std::cout << "IMPOSSIBLE" << std::endl;
    }
  } else {
    double equilibrium = (double)x / den;
    printf("%0.9f\n", equilibrium);
  }
}
