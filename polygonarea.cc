#include <cmath>
#include <iostream>
#include <vector>

int
main()
{
  int n;
  while (std::cin >> n && n) {
    std::vector<int> xs(n);
    std::vector<int> ys(n);
    for (int i = 0; i < n; ++i) {
      std::cin >> xs[i] >> ys[i];
    }
    int windage = 0;
    for (int i = 0; i < n; ++i) {
      int j = (i + 1) % n;
      int a = xs[i] * ys[j];
      int b = xs[j] * ys[i];
      windage += a - b;
    }
    bool ccw = windage > 0;
    double a = std::abs(windage / 2.0);
    printf("%s %0.1f\n", ccw ? "CCW" : "CW", a);
  }
}