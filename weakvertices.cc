#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

int
main()
{
  int n;
  while (std::cin >> n && n != -1) {
    char const* sep = "";
    std::vector<int> adj(n * n);
    std::copy_n(std::istream_iterator<int>(std::cin), n * n, adj.begin());
    for (int v = 0; v < n; ++v) {
      bool strong = false;
      for (int v1 = 0; v1 < n; ++v1) {
        if (v1 == v)
          continue;
        if (!adj[v1 * n + v])
          continue;
        for (int v2 = v1 + 1; v2 < n; ++v2) {
          if (adj[v2 * n + v] && adj[v2 * n + v1]) {
            strong = true;
            break;
          }
        }
        if (strong)
          break;
      }
      if (!strong) {
        std::cout << sep << v;
        sep = " ";
      }
    }
    std::cout << std::endl;
  }
}
