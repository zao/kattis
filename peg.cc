#include <iostream>
#include <string>

int
main()
{
  int jumpLeft[7][7] = {
    { 0, 0, 0, 0, 1, 0, 0 }, // xxoooxx
    { 0, 0, 0, 0, 1, 0, 0 }, // xxoooxx
    { 0, 0, 1, 1, 1, 1, 1 }, // ooooooo
    { 0, 0, 1, 1, 1, 1, 1 }, // ooooooo
    { 0, 0, 1, 1, 1, 1, 1 }, // ooooooo
    { 0, 0, 0, 0, 1, 0, 0 }, // xxoooxx
    { 0, 0, 0, 0, 1, 0, 0 }, // xxoooxx
  };
  int jumpRight[7][7] = {
    { 0, 0, 1, 0, 0, 0, 0 }, // xxoooxx
    { 0, 0, 1, 0, 0, 0, 0 }, // xxoooxx
    { 1, 1, 1, 1, 1, 0, 0 }, // ooooooo
    { 1, 1, 1, 1, 1, 0, 0 }, // ooooooo
    { 1, 1, 1, 1, 1, 0, 0 }, // ooooooo
    { 0, 0, 1, 0, 0, 0, 0 }, // xxoooxx
    { 0, 0, 1, 0, 0, 0, 0 }, // xxoooxx
  };
  int jumpUp[7][7] = {
    { 0, 0, 0, 0, 0, 0, 0 }, // xxoooxx
    { 0, 0, 0, 0, 0, 0, 0 }, // xxoooxx
    { 0, 0, 1, 1, 1, 0, 0 }, // ooooooo
    { 0, 0, 1, 1, 1, 0, 0 }, // ooooooo
    { 1, 1, 1, 1, 1, 1, 1 }, // ooooooo
    { 0, 0, 1, 1, 1, 0, 0 }, // xxoooxx
    { 0, 0, 1, 1, 1, 0, 0 }, // xxoooxx
  };
  int jumpDown[7][7] = {
    { 0, 0, 1, 1, 1, 0, 0 }, // xxoooxx
    { 0, 0, 1, 1, 1, 0, 0 }, // xxoooxx
    { 1, 1, 1, 1, 1, 1, 1 }, // ooooooo
    { 0, 0, 1, 1, 1, 0, 0 }, // ooooooo
    { 0, 0, 1, 1, 1, 0, 0 }, // ooooooo
    { 0, 0, 0, 0, 0, 0, 0 }, // xxoooxx
    { 0, 0, 0, 0, 0, 0, 0 }, // xxoooxx
  };

  char board[7][7] = {};
  std::string line;
  for (int row = 0; row < 7; ++row) {
    std::getline(std::cin, line);
    std::copy(line.begin(), line.begin() + 7, board[row]);
  }
  int moves = 0;
  for (int row = 0; row < 7; ++row) {
    for (int col = 0; col < 7; ++col) {
      if (board[row][col] != 'o')
        continue;
      if (jumpLeft[row][col] && board[row][col - 1] == 'o' &&
          board[row][col - 2] == '.')
        ++moves;
      if (jumpRight[row][col] && board[row][col + 1] == 'o' &&
          board[row][col + 2] == '.')
        ++moves;
      if (jumpUp[row][col] && board[row - 1][col] == 'o' &&
          board[row - 2][col] == '.')
        ++moves;
      if (jumpDown[row][col] && board[row + 1][col] == 'o' &&
          board[row + 2][col] == '.')
        ++moves;
    }
  }
  std::cout << moves << std::endl;
}
