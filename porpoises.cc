#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

struct mat2
{
  uint64_t m11, m12;
  uint64_t m21, m22;
};

std::ostream&
operator<<(std::ostream& os, mat2 m)
{
  return os << "[" << m.m11 << " " << m.m12 << ";" << m.m21 << " " << m.m22
            << "]";
}

mat2 operator*(mat2 a, mat2 b)
{
  mat2 ret = {
    (a.m11 * b.m11 + a.m12 * b.m21), (a.m11 * b.m12 + a.m12 * b.m22),
    (a.m21 * b.m11 + a.m22 * b.m21), (a.m21 * b.m12 + a.m22 * b.m22),
  };
  return ret;
}

mat2
Modulo(mat2 a, uint64_t modulo)
{
  mat2 ret = {
    a.m11 % modulo, a.m12 % modulo, a.m21 % modulo, a.m22 % modulo,
  };
  return ret;
}

int indent = 0;

mat2
PowModulo(mat2 a, uint64_t modulo, uint32_t power)
{
  mat2 ret = a;
  if (power > 1) {
    mat2 am = PowModulo(a, modulo, power / 2);
    if (power % 2) {
      ret = Modulo(a * Modulo(am * am, modulo), modulo);
    } else {
      ret = Modulo(am * am, modulo);
    }
  }
  return ret;
}

int
main()
{
  uint64_t modulo = 1000000000ull;
  uint64_t period = 1500000000ull; // 15 * 10^{9-1} for 9 digits
  int n;
  std::cin >> n;
  for (int cse = 0; cse < n; ++cse) {
    int tag;
    uint64_t years;
    std::cin >> tag >> years;
    years %= period;
    mat2 m = { 1, 1, 1, 0 };
    m = PowModulo(m, modulo, years);
    std::cout << tag << " " << m.m12 << std::endl;
  }
}
