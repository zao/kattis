#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

int
main()
{
  int n;
  while (std::cin >> n && n) {
    using Dish = std::string;
    using Name = std::string;
    std::map<Dish, std::set<Name>> history;
    std::string line;
    for (int i = 0; i < n; ++i) {
      Name name;
      std::cin >> name;
      std::getline(std::cin, line);
      std::istringstream iss(line);
      Dish dish;
      while (iss >> dish) {
        history[dish].insert(name);
      }
    }
    for (auto const& p : history) {
      std::cout << p.first;
      for (auto& name : p.second) {
        std::cout << " " << name;
      }
      std::cout << std::endl;
    }
    std::cout << std::endl;
  }
}
