#include <fstream>
#include <iostream>
#include <random>

int
main(int argc, char* argv[])
{
  if (argc == 3) {
    int count = std::atoi(argv[1]);
    std::string base = argv[2];
    for (int k = 1; k <= count; ++k) {
      int n = 1 + (std::rand() % 30);
      std::ofstream os(base + "." + std::to_string(k) + ".in");
      for (int i = 0; i < n; ++i) {
        os << (std::rand() % 4);
      }
      os << std::endl;
    }
  }
}
