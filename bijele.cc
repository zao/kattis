#include <iostream>

int main()
{
  int desired[] = {1, 1, 2, 2, 2, 8};
  for (int i = 0; i < 6; ++i) {
    int given;
    std::cin >> given;
    std::cout << (desired[i] - given) << " ";
  }
  std::cout << std::endl;
}
