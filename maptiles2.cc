#include <iostream>
#include <string>

int
main()
{
  std::string line;
  std::cin >> line;
  uint32_t x = 0, y = 0;
  for (auto ch : line) {
    x <<= 1;
    y <<= 1;
    uint32_t code = ch - '0';
    x |= code & 1;
    y |= (code & 3) >> 1;
  }
  std::cout << line.size() << " " << x << " " << y << std::endl;
}
