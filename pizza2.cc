#include <iostream>

int
main()
{
  int c, r;
  std::cin >> r >> c;
  double innerPart = (r - c) * (r - c);
  double wholePizza = r * r;
  char buf[20];
  sprintf(buf, "%0.9f", 100.0 * innerPart / wholePizza);
  std::cout << buf << std::endl;
}
