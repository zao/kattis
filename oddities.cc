#include <cmath>
#include <iostream>

int main() {
  int n;
  std::cin >> n;
  for (int i = 0; i < n; ++i) {
    int val;
    std::cin >> val;
    std::cout << val << ((std::abs(val)%2 == 1) ? " is odd" : " is even") << std::endl;
  }
}
