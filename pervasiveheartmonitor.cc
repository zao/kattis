#include <iostream>
#include <sstream>
#include <string>

int
main()
{
  std::string line;
  while (std::getline(std::cin, line)) {
    std::string name;
    std::istringstream iss(line);
    int count = 0;
    double sum = 0.0;
    std::string token;
    while (iss >> token) {
      if (isdigit(token[0])) {
        sum += std::stod(token);
        ++count;
      } else {
        name += " " + token;
      }
    }
    printf("%0.9f%s\n", sum / count, name.c_str());
  }
}
