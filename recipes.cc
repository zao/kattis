#include <iostream>
#include <tuple>
#include <vector>

struct Ingredient
{
  std::string name;
  double weight;
  double percentage;
};

int
main()
{
  int t;
  std::cin >> t;
  for (int cse = 1; cse <= t; ++cse) {
    double r, p, d;
    std::cin >> r >> p >> d;
    int mainIdx;
    std::vector<Ingredient> ingredients(r);
    for (int i = 0; i < r; ++i) {
      std::cin >> ingredients[i].name >> ingredients[i].weight >>
        ingredients[i].percentage;
      if (ingredients[i].percentage == 100.0) {
        mainIdx = i;
      }
    }
    double scale = (double)d / p;
    double mainWeight = ingredients[mainIdx].weight * scale;
    printf("Recipe # %d\n", cse);
    for (int i = 0; i < r; ++i) {
      double adjWeight = mainWeight * (ingredients[i].percentage / 100.0);
      printf("%s %0.1f\n", ingredients[i].name.c_str(), adjWeight);
    }
    printf("----------------------------------------\n");
  }
}
