#include <iostream>
#include <map>
#include <stdint.h>

int
main()
{
  std::map<int, int64_t> rolls[2];
  int64_t ns[2] = {};
  int low[2], high[2];
  for (int player = 0; player < 2; ++player) {
    std::cin >> low[0] >> high[0] >> low[1] >> high[1];
    for (int d0 = low[0]; d0 <= high[0]; ++d0) {
      for (int d1 = low[1]; d1 <= high[1]; ++d1) {
        rolls[player][d0 + d1]++;
        ++ns[player];
      }
    }
  }
  int64_t den = ns[0] * ns[1];
  for (int player = 0; player < 2; ++player) {
    for (auto& p : rolls[player]) {
      p.second *= ns[1 - player];
    }
  }
  int64_t probAcc[2] = {};
  int64_t tie;
  for (auto I0 = rolls[0].begin(); I0 != rolls[0].end(); ++I0) {
    for (auto I1 = rolls[1].begin(); I1 != rolls[1].end(); ++I1) {
      int d0 = I0->first;
      int d1 = I1->first;
      int64_t prob = I0->second * I1->second;
      if (d0 < d1) {
        probAcc[1] += prob;
      } else if (d0 > d1) {
        probAcc[0] += prob;
      } else {
        tie += prob;
      }
    }
  }
  if (probAcc[0] < probAcc[1]) {
    std::cout << "Emma" << std::endl;
  } else if (probAcc[0] > probAcc[1]) {
    std::cout << "Gunnar" << std::endl;
  } else {
    std::cout << "Tie" << std::endl;
  }
}
