#include <iostream>

int
main()
{
  int val;
  std::cin >> val;
  int acc = 0;
  while (val) {
    int bit = val & 1;
    val >>= 1;
    acc <<= 1;
    acc |= bit;
  }
  std::cout << acc << std::endl;
}
