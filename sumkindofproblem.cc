#include <iostream>

int
main()
{
  int p;
  for (std::cin >> p; p; --p) {
    int k, n;
    std::cin >> k >> n;
    int psum = 0, osum = 0, esum = 0;
    for (int i = 0; i < n; ++i) {
      psum += i + 1;
      osum += 2 * i + 1;
      esum += 2 * (i + 1);
    }
    std::cout << k << " " << psum << " " << osum << " " << esum << std::endl;
  }
}
