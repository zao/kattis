#include <iostream>
#include <map>
#include <string>

struct Record {
  int solveTime = 0;
  bool isSolved = false;
  int wrongs = 0;
};

int main() {
  std::map<char, Record> records;
  do {
    int t;
    std::cin >> t;
    if (t == -1) break;
    char id;
    std::string result;
    std::cin >> id >> result;
    bool failed = (result == "wrong");
    auto& rec = records[id];
    if (!rec.isSolved) {
      if (failed) {
        rec.wrongs++;
      }
      else {
        rec.solveTime = t;
        rec.isSolved = true;
      }
    }
  } while (1);
  int solved = 0;
  int ranking = 0;
  for (auto p : records) {
    auto& rec = p.second;
    if (rec.isSolved) {
      ++solved;
      ranking += rec.solveTime + rec.wrongs * 20;
    }
  }
  std::cout << solved << " " << ranking << std::endl;
}
