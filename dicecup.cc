#include <iostream>
#include <map>

int main() {
  std::map<int, int> counts;
  int n, m;
  std::cin >> n >> m;

  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= m; ++j) {
      counts[i+j]++;
    }
  }

  int maxVal = 0;
  for (auto p : counts) {
    if (p.second > maxVal) {
      maxVal = p.second;
    }
  }
  for (auto p : counts) {
    if (p.second == maxVal) {
      std::cout << p.first << std::endl;
    }
  }
}
