#include <iostream>
#include <set>

int
main()
{
  int n;
  std::cin >> n;
  std::multiset<int> ts;
  for (int i = 1; i <= n; ++i) {
    int t;
    std::cin >> t;
    ts.insert(t);
  }
  int highest = 0;
  for (auto I = ts.begin(); I != ts.end(); ++I) {
    int doneDay = *I + n;
    if (highest < doneDay)
      highest = doneDay;
    --n;
  }
  std::cout << (highest + 1) << std::endl;
}
