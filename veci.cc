#include <algorithm>
#include <iostream>
#include <iterator>

int
main()
{
  int num[6] = {};
  char ch;
  int i = 0;
  while (std::cin >> ch && std::isdigit(ch)) {
    num[i] = ch - '0';
    ++i;
  }
  if (std::next_permutation(num, num + i)) {
    std::copy_n(num, i, std::ostream_iterator<int>(std::cout));
    std::cout << std::endl;
  } else {
    std::cout << 0 << std::endl;
  }
}
