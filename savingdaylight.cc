#include <cstdio>
#include <iostream>
#include <string>

int
main()
{
  std::string month, day, year, from, to;
  while (std::cin >> month >> day >> year >> from >> to) {
    int h, m;
    sscanf(to.c_str(), "%d:%d", &h, &m);
    int minutes = h * 60 + m;
    sscanf(from.c_str(), "%d:%d", &h, &m);
    minutes -= h * 60 + m;
    int hours = minutes / 60;
    minutes %= 60;
    printf("%s %s %s %d hours %d minutes\n", month.c_str(), day.c_str(),
           year.c_str(), hours, minutes);
  }
}
