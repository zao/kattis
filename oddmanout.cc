#include <iostream>
#include <set>

int
main()
{
  int n;
  std::cin >> n;
  for (int cse = 1; cse <= n; ++cse) {
    int g;
    std::cin >> g;
    std::set<int> uncertain;
    for (int i = 0; i < g; ++i) {
      int id;
      std::cin >> id;
      auto I = uncertain.find(id);
      if (I != uncertain.end()) {
        uncertain.erase(I);
      } else {
        uncertain.insert(id);
      }
    }
    std::cout << "Case #" << cse << ": " << *uncertain.begin() << std::endl;
  }
}
