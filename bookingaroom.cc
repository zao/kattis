#include <iostream>
#include <vector>

int
main()
{
  int r, n;
  std::cin >> r >> n;
  bool booked[101] = {};
  if (r == n) {
    std::cout << "too late" << std::endl;
    return 0;
  }
  for (int i = 0; i < n; ++i) {
    int num;
    std::cin >> num;
    booked[num - 1] = true;
  }
  for (int i = 0; i < r; ++i) {
    if (!booked[i]) {
      std::cout << (i + 1) << std::endl;
      return 0;
    }
  }
}
