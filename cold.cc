#include <iostream>

int main()
{
  int n;
  std::cin >> n;
  int coldDays = 0;
  for (int i = 0; i < n; ++i)
  {
    int temp;
    std::cin >> temp;
    if (temp < 0)
      ++coldDays;
  }
  std::cout << coldDays << std::endl;
}
