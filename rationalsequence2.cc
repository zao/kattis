#include <iostream>
#include <stdint.h>
#include <vector>

struct Rational
{
  Rational() {}
  Rational(uint32_t p, uint32_t q)
    : p(p)
    , q(q)
  {
  }
  uint32_t p, q;
};

/*
  Givet ett barn p' / q', en av följande identiteter håller:
  V: p' = p
     q' = p + q
  H: p' = p + q
     q' = q

  Då alla tal är positiva så bestäms valet av vilken av p' och q'
  som är störst.

  Stigen från lövnod till roten hittas genom att stegvis lösa
  förälderns p och q samt notera varje val i en lista.

  ###
  1 / 3 = p / (p + q)  [vänster]
  1 / 3 = (p + q) / q  [höger]

  1 / (1 + 2)
  dvs. barn vänster åt
  1 / 2 = p / (p + q)  [vänster]
  1 / 2 = (p + q) / q  [höger]

  1 / (1 + 1)
  dvs. barn vänster åt
  1 / 1  [rot]

  ?? vänstersväng ger N' = 2N
  ?? högersväng ger N' = 2N + 1

  [V, V]
  N = [1, 2, 4]

###
  5 / 2 = p / (p + q)  [vänster]
  5 / 2 = (p + q) / q  [höger]

  (3 + 2) / 2
  dvs. barn höger åt
  3 / 2 = p / (p + q)  [vänster]
  3 / 2 = (p + q) / q  [höger]

  (1 + 2) / 2
  dvs. barn höger åt
  1 / 2 = p / (p + q)  [vänster]
  1 / 2 = (p + q) / q  [höger]

  1 / (1 + 1)
  dvs. barn vänster åt
  1 / 1  [rot]

  [V H H]
  N = [1, 2, 5, 11]

*/

bool const Left = false;
bool const Right = true;

std::vector<bool>
SolvePath(Rational r)
{
  std::vector<bool> ret;
  while (r.p != 1 || r.q != 1) {
    if (r.p < r.q) {
      // vänster, r = p / (p + q)
      r = { r.p, r.q - r.p };
      ret.push_back(Left);
    } else {
      // höger, r = (p + q) / q
      r = { r.p - r.q, r.q };
      ret.push_back(Right);
    }
  }
  return ret;
}

int
main()
{
  int p;
  scanf("%d\n", &p);
  for (int cse = 1; cse <= p; ++cse) {
    int setId;
    Rational r;
    scanf("%d %u/%u\n", &setId, &r.p, &r.q);
    std::vector<bool> path = SolvePath(r);
    uint32_t n = 1;
    for (auto I = path.rbegin(); I != path.rend(); ++I) {
      n += (*I == Left ? n : n + 1);
    }
    printf("%d %u\n", setId, n);
  }
}
