#include <iostream>
#include <string>

int
main()
{
  int n;
  for (std::cin >> n; n; --n) {
    std::string a, b;
    std::cin >> a >> b;
    std::cout << a << std::endl << b << std::endl;
    int len = a.size();
    for (int i = 0; i < len; ++i) {
      std::cout << (a[i] != b[i] ? '*' : '.');
    }
    std::cout << std::endl << std::endl;
  }
}
