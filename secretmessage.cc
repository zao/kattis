#include <cmath>
#include <iostream>
#include <string>
#include <vector>

int
main()
{
  int n;
  std::cin >> n;
  std::cin.ignore(100, '\n');
  for (int cse = 0; cse < n; ++cse) {
    std::string line;
    std::getline(std::cin, line);
    double len = line.size();
    int side = (int)std::ceil(std::sqrt(len));
    std::vector<char> box(side * side, '*');
    std::copy(line.begin(), line.end(), box.begin());
    for (int col = 0; col < side; ++col) {
      for (int row = side - 1; row >= 0; --row) {
        char ch = box[col + row * side];
        if (ch != '*')
          std::cout << ch;
      }
    }
    std::cout << std::endl;
  }
}
