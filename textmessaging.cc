#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

#include <inttypes.h>

int
main()
{
  int n;
  std::cin >> n;
  for (int cse = 1; cse <= n; ++cse) {
    int p, k, l;
    std::cin >> p >> k >> l;
    std::vector<uint64_t> freq(l);
    std::copy_n(std::istream_iterator<uint64_t>(std::cin), l, freq.begin());
    std::sort(freq.begin(), freq.end(), std::greater<uint64_t>{});
    uint64_t sum = 0;
    for (int i = 0; i < l; ++i) {
      uint64_t depth = 1 + (i / k);
      sum += depth * freq[i];
    }
    printf("Case #%d: %" PRId64 "\n", cse, sum);
  }
}
