#include <iostream>

int
main()
{
  int n;
  while (std::cin >> n && n != -1) {
    int distSum = 0;
    int prevTime = 0;
    for (int i = 0; i < n; ++i) {
      int speed, time;
      std::cin >> speed >> time;
      int dt = time - prevTime;
      distSum += dt * speed;
      prevTime = time;
    }
    std::cout << distSum << " miles" << std::endl;
  }
}
