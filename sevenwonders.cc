#include <algorithm>
#include <iostream>
#include <string>

int
main()
{
  int tablets = 0, compasses = 0, gears = 0;

  std::string played;
  std::cin >> played;
  for (auto ch : played) {
    if (ch == 'T')
      tablets++;
    if (ch == 'C')
      compasses++;
    if (ch == 'G')
      gears++;
  }
  int sets = std::min(tablets, std::min(compasses, gears));
  int score =
    tablets * tablets + compasses * compasses + gears * gears + 7 * sets;
  std::cout << score << std::endl;
}
