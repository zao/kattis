#include <iostream>

int
main()
{
  int num, den;
  while (std::cin >> num >> den && den) {
    int whole = num / den;
    num %= den;
    std::cout << whole << " " << num << " / " << den << std::endl;
  }
}
