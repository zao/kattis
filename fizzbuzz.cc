#include <iostream>

int
main()
{
  int x, y, n;
  std::cin >> x >> y >> n;
  for (int i = 1; i <= n; ++i) {
    bool multX = i % x == 0;
    bool multY = i % y == 0;
    if (multX)
      std::cout << "Fizz";
    if (multY)
      std::cout << "Buzz";
    if (!multX && !multY) {
      std::cout << i;
    }
    std::cout << std::endl;
  }
}
