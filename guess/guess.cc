#include <iostream>

int
main()
{
  int low = 1, high = 1000;
  do {
    int guess = (low + high) / 2;
    std::cout << guess << std::endl;
    std::string reply;
    std::cin >> reply;
    if (reply == "correct") {
      break;
    } else if (reply == "lower") {
      high = guess - 1;
    } else if (reply == "higher") {
      low = guess + 1;
    }
  } while (1);
}
