#include <cmath>
#include <iostream>

int
main()
{
  double a;
  std::cin >> a;
  printf("%0.15f\n", 4.0 * std::sqrt((double)a));
}
