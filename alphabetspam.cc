#include <iostream>
#include <string>

int
main()
{
  std::string line;
  std::getline(std::cin, line);
  double whitespace, lowercase, uppercase, symbols;
  double total = line.size();
  for (auto ch : line) {
    if (ch >= 'A' && ch <= 'Z')
      uppercase += 1;
    else if (ch >= 'a' && ch <= 'z')
      lowercase += 1;
    else if (ch == '_')
      whitespace += 1;
    else
      symbols += 1;
  }
  printf("%0.10f\n%0.10f\n%0.10f\n%0.10f\n", whitespace / total,
         lowercase / total, uppercase / total, symbols / total);
}
