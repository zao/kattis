#include <iostream>

int
main()
{
  double c, l;
  std::cin >> c >> l;
  double total = 0.0;
  for (int i = 0; i < l; ++i) {
    double w, h;
    std::cin >> w >> h;
    total += c * w * h;
  }
  printf("%0.9f\n", total);
}
