#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

int
main()
{
  int n;
  int set = 1;
  while (std::cin >> n && n) {
    std::vector<std::string> names;
    names.reserve(n);
    std::copy_n(std::istream_iterator<std::string>(std::cin), n,
                std::back_inserter(names));
    std::cout << "SET " << set << std::endl;
    for (int i = 0; i < n; i += 2) {
      std::cout << names[i] << std::endl;
    }
    int last = (n % 2) ? (n - 2) : (n - 1);
    for (int i = last; i >= 0; i -= 2) {
      std::cout << names[i] << std::endl;
    }
    ++set;
  }
}
