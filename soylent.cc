#include <cmath>
#include <iostream>

int
main()
{
  int n;
  std::cin >> n;
  for (int i = 0; i < n; ++i) {
    int calsNeeded;
    std::cin >> calsNeeded;
    double bottles = std::ceil(calsNeeded / 400.0);
    std::cout << bottles << std::endl;
  }
}
