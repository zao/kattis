#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

enum Color
{
  Blue,
  Red
};

int
main()
{
  int n;
  std::cin >> n;
  for (int cse = 0; cse < n; ++cse) {
    int s;
    std::cin >> s;
    std::vector<int> blues, reds;
    int allBlues = 0, allReds = 0;
    for (int i = 0; i < s; ++i) {
      std::string token;
      std::cin >> token;
      int len;
      char color;
      sscanf(token.c_str(), "%d%c", &len, &color);
      if (color == 'B') {
        blues.push_back(len - 1);
        allBlues += len - 1;
      } else {
        reds.push_back(len - 1);
        allReds += len - 1;
      }
    }
    int nBlues = blues.size();
    int nReds = reds.size();
    int sum = 0;
    if (nBlues == 0 || nReds == 0) {
      sum = 0;
    } else if (nBlues > nReds) {
      auto I = blues.begin() + nReds;
      std::nth_element(blues.begin(), I, blues.end(), std::greater<int>());
      sum = allReds + std::accumulate(blues.begin(), I, 0);
    } else if (nBlues < nReds) {
      auto I = reds.begin() + nBlues;
      std::nth_element(reds.begin(), I, reds.end(), std::greater<int>());
      sum = allBlues + std::accumulate(reds.begin(), I, 0);
    } else {
      sum = allBlues + allReds;
    }
    printf("Case #%d: %d\n", cse + 1, sum);
  }
}
