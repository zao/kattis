#include <iostream>

int
main()
{
  int t;
  std::cin >> t;
  for (int cse = 0; cse < t; ++cse) {
    int k;
    std::cin >> k;
    int count = 0;
    while (k) {
      count = count * 2 + 1;
      --k;
    }
    std::cout << count << std::endl;
  }
}
