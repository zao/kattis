#include <iostream>
#include <string>

enum Policy
{
  UpPolicy,
  DownPolicy,
  PreferencePolicy,
};

enum
{
  Up,
  Down,
};

int
main()
{
  std::string seq;
  std::cin >> seq;
  int init = (seq[0] == 'U' ? Up : Down);
  int cur[3] = { init, init, init };
  int adj[3] = {};
  for (int i = 1; i < seq.size(); ++i) {
    int next = seq[i] == 'U' ? Up : Down;
    // Pay for incoming change
    if (cur[UpPolicy] != next)
      ++adj[UpPolicy];
    if (cur[DownPolicy] != next)
      ++adj[DownPolicy];
    if (cur[PreferencePolicy] != next)
      ++adj[PreferencePolicy];

    cur[UpPolicy] = Up;
    cur[DownPolicy] = Down;
    cur[PreferencePolicy] = (next == Up ? Up : Down);

    if (next != cur[UpPolicy])
      ++adj[UpPolicy];
    if (next != cur[DownPolicy])
      ++adj[DownPolicy];
  }
  for (int i = 0; i < 3; ++i) {
    std::cout << adj[i] << std::endl;
  }
}
