#include <iostream>

int
DigitSum(int val)
{
  int sum = 0;
  while (val) {
    sum += val % 10;
    val /= 10;
  }
  return sum;
}

int
main()
{
  int n;
  while (std::cin >> n && n) {
    for (int i = 11;; ++i) {
      int nSum = DigitSum(n);
      int inSum = DigitSum(i * n);
      if (nSum == inSum) {
        std::cout << i << std::endl;
        break;
      }
    }
  }
}
