#include <iostream>
#include <set>
#include <string>

int
main()
{
  int n;
  std::cin >> n;
  using Name = std::string;
  std::set<Name> inside;
  for (int i = 0; i < n; ++i) {
    std::string what;
    Name who;
    std::cin >> what >> who;
    if (what == "entry") {
      std::cout << who << " entered";
      if (inside.count(who)) {
        std::cout << " (ANOMALY)";
      }
      inside.insert(who);
      std::cout << std::endl;
    } else {
      std::cout << who << " exited";
      if (!inside.count(who)) {
        std::cout << " (ANOMALY)";
      }
      inside.erase(who);
      std::cout << std::endl;
    }
  }
}
