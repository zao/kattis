#include <cmath>
#include <iostream>

double const pi = 3.14159265359;

int
main()
{
  double a, b, s, m, n;
  while (std::cin >> a >> b >> s >> m >> n && a) {
    double dx = a * m, dy = b * n;
    double d = std::sqrt(dx * dx + dy * dy);
    double v = d / s;
    double ang = 180.0 * atan2(dy, dx) / pi;
    printf("%0.2f %0.2f\n", ang, v);
  }
}
