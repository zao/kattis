#include <algorithm>
#include <iostream>
#include <map>

struct Pair
{
  int l, r;
};

bool
operator<(Pair a, Pair b)
{
  if (a.l != b.l)
    return a.l < b.l;
  return a.r < b.r;
}

std::map<Pair, int> scoreMemo;

int
Solve(Pair pair)
{
  if (pair.l == 0 && pair.r == 0)
    return 0;
  auto I = scoreMemo.find(pair);
  if (I != scoreMemo.end())
    return I->second;
  int score = 0;
  for (int i = 0; i < pair.l; ++i) {
    int nr = pair.l - i - 1;
    if (i <= nr) {
      score = (std::max)(score, 1 + Solve({ i, nr }));
    }
  }
  for (int i = 0; i < pair.r; ++i) {
    int nr = pair.r - i - 1;
    if (i <= nr) {
      score = (std::max)(score, 1 + Solve({ i, nr }));
    }
  }
  scoreMemo[pair] = score;
  return score;
}

int
main()
{
  int a, b, c;
  std::cin >> a >> b >> c;
  int l = b - a - 1;
  int r = c - b - 1;
  int count = Solve({ l, r });
  std::cout << count << std::endl;
}
