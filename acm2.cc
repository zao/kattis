#include <algorithm>
#include <iostream>
#include <set>
#include <utility>
#include <vector>

using Solved = int;
using Penalty = int;
using Result = std::pair<Solved, Penalty>;

Result
Solve(Result lastStage, int usedTime, std::set<int> estimates)
{
  Result score = lastStage;
  for (auto I = estimates.begin(); I != estimates.end(); ++I) {
    int cost = *I;
    usedTime += cost;
    if (usedTime > 300)
      break;
    score.first++;
    score.second += usedTime;
  }
  return score;
}

int
main()
{
  int n, p;
  std::cin >> n >> p;

  int base = 0;
  std::set<int> times;
  for (int i = 0; i < n; ++i) {
    int est;
    std::cin >> est;
    if (i == p) {
      base = est;
      if (base > 300) {
        std::cout << "0 0\n";
        return 0;
      }
    } else
      times.insert(est);
  }
  Result res = { 1, base };
  res = Solve(res, base, times);
  std::cout << res.first << " " << res.second << std::endl;
}
