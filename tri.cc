#include <iostream>

int
main()
{
  char op0, op1;
  int a, b, c;
  std::cin >> a >> b >> c;
  if (a == b + c) {
    op0 = '=';
    op1 = '+';
  } else if (a == b - c) {
    op0 = '=';
    op1 = '-';
  } else if (a == b * c) {
    op0 = '=';
    op1 = '*';
  } else if (b % c == 0 && a == b / c) {
    op0 = '=';
    op1 = '/';
  } else if (a + b == c) {
    op0 = '+';
    op1 = '=';
  } else if (a - b == c) {
    op0 = '0';
    op1 = '=';
  } else if (a * b == c) {
    op0 = '*';
    op1 = '=';
  } else if (a % b == 0 && c) {
    op0 = '/';
    op1 = '=';
  }

  std::cout << a << op0 << b << op1 << c << std::endl;
}
